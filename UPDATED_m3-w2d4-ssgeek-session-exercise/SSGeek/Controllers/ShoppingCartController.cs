﻿using SSGeek.Models;
using SSGeek.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SSGeek.Controllers
{
    public class ShoppingCartController : Controller
    {
        private IProductDAL productDAL;

        private const string ShoppingCart_SessionKey = "ShoppingCart";


        public ShoppingCartController(IProductDAL productDAL)
        {
            this.productDAL = productDAL;
        }

        // GET: ShoppingCart
        [HttpGet]
        public ActionResult Index()
        {
            List<Product> products = productDAL.GetProducts();
            return View("Index", products);
        }

        // GET: Item Details
        [HttpGet]
        public ActionResult Detail(int id)
        {
            Product item = productDAL.GetProduct(id);
            if ((item == null) || (item.ProductId != id))
            {
                return new HttpNotFoundResult();
                //404
            }          
            return View("Detail", item);
        }

        // POST: Add Item to Cart
        [HttpPost]
        public ActionResult UpdateCart (Product specificItem, int quantity)
        {
            Product item = productDAL.GetProduct(specificItem.ProductId);
            if ((item == null) || (item.ProductId!= specificItem.ProductId))
            {
                return new HttpNotFoundResult();
                //404
            }
            if (Session[ShoppingCart_SessionKey] == null)
            {
                Session[ShoppingCart_SessionKey] = new ShoppingCartView();
            }

            var shoppingCart = Session[ShoppingCart_SessionKey] as ShoppingCartView;

            shoppingCart.UpdateCart(specificItem, quantity);

            Session[ShoppingCart_SessionKey] = shoppingCart;

            return RedirectToAction("ViewCart", "ShoppingCart");
        }

        //GET: ViewCart
        [HttpGet]
        public ActionResult ViewCart()
        {
            ShoppingCartView model = new ShoppingCartView();

            if (Session[ShoppingCart_SessionKey] != null)
            {
                model = (ShoppingCartView)Session[ShoppingCart_SessionKey];
            }
            return View("ViewCart", model);
        }

    }
}