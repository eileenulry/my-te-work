﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SSGeek.Models;
using System.Data.SqlClient;

namespace SSGeek.DAL
{
    public class ProductSqlDAL : IProductDAL
    {
        public readonly string connectionString;

        private string sql_GetProducts = @"SELECT * FROM products";
        private string sql_GetByProductId = @"SELECT * FROM products WHERE product_id = @productId";

        public ProductSqlDAL(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public Product GetProduct(int id)
        {
            //List<Product> products = GetProducts();

            Product item = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_GetByProductId);
                    cmd.Connection = conn;

                    cmd.Parameters.AddWithValue("@productId", id);

                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        item = MapProductFromReader(reader);
                    }
                }
            }
            catch (SqlException ex)
            {
                throw;
            }

            //foreach (Product product in Models)
            //{
            //    if (product.ProductId == id)
            //    {
            //        item = product;
            //    }
            //    return product;
            //}
            return item;
        }
       
        public List<Product> GetProducts()
        {
            List<Product> products = new List<Product>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = sql_GetProducts;
                    cmd.Connection = conn;

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Product product = MapProductFromReader(reader);
                        products.Add(product);
                    }

                }
            }
            catch (SqlException ex)
            {
                throw;
            }

            return products;
        }
        //public bool Update(Product product)
        //{
        //    try
        //    {
        //        using (SqlConnection conn = new SqlConnection(connectionString))
        //        {
        //            conn.Open();

        //            SqlCommand cmd = new SqlCommand(sql_GetProducts, conn);

        //            cmd.Parameters.AddWithValue("@product_id", product.ProductId);
        //            cmd.Parameters.AddWithValue("@name", product.Name);
        //            cmd.Parameters.AddWithValue("@description", product.Description);
        //            cmd.Parameters.AddWithValue("@price", product.Price);
        //            cmd.Parameters.AddWithValue("@image_name", product.ImageName);                   
        //            int rowsAffected = cmd.ExecuteNonQuery();

        //            return rowsAffected > 0;
        //        }
        //    }
        //    catch (SqlException ex)
        //    {
        //        // Error Logging that a problem occurred, don't show the user
        //        throw;
        //    }
        //}
        private Product MapProductFromReader(SqlDataReader reader)
        {
            return new Product()
            {
                ProductId = Convert.ToInt32(reader["product_id"]),
                Name = Convert.ToString(reader["name"]),    
                Description = Convert.ToString(reader["description"]),
                Price = Convert.ToDouble(reader["price"]),
                ImageName = Convert.ToString(reader["image_name"]),
            };

    }

    }


}
